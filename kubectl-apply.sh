#!/bin/bash
# Files are ordered in proper order with needed wait for the dependent custom resource definitions to get initialized.
# Usage: bash kubectl-apply.sh
args=("$@")

logSummary(){
    echo ""
    echo "#####################################################"
    echo "Please find the below useful endpoints,"
    echo "Image: ${args[0]}"
    echo "jhipsterImmo - http://jhipsterimmo.prod-immo.cloud-wd29.fr"
    echo "#####################################################"
}

kubectl apply -f namespace.yml

# read the yml template from a file and substitute the string 
# {{IMAGENAME}} with the value of the MYVARVALUE variable
template=`cat "jhipsterimmo-deployment.yml.tpl" | sed "s/{{IMAGENAME}}/${args[0]}/g"`
# apply the yml with the substituted value
echo "$template" | kubectl apply -f -

kubectl apply -f jhipsterimmo/

logSummary
