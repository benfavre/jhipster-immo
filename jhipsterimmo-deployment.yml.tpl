apiVersion: apps/v1
kind: Deployment
metadata:
  name: jhipsterimmo
  namespace: prod-immo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: jhipsterimmo
      version: 'v1'
  template:
    metadata:
      labels:
        app: jhipsterimmo
        version: 'v1'
    spec:
      initContainers:
        - name: init-ds
          image: busybox:latest
          command:
            - '/bin/sh'
            - '-c'
            - |
              while true
              do
                rt=$(nc -z -w 1 jhipsterimmo-postgresql 5432)
                if [ $? -eq 0 ]; then
                  echo "DB is UP"
                  break
                fi
                echo "DB is not yet reachable;sleep for 10s before retry"
                sleep 10
              done
      containers:
        - name: jhipsterimmo-app
          image: {{IMAGENAME}}
          env:
            - name: SPRING_PROFILES_ACTIVE
              value: prod
            - name: JHIPSTER_SECURITY_AUTHENTICATION_JWT_BASE64_SECRET
              valueFrom:
                secretKeyRef:
                  name: jwt-secret
                  key: secret
            - name: SPRING_DATASOURCE_URL
              value: jdbc:postgresql://jhipsterimmo-postgresql.prod-immo.svc.cluster.local:5432/jhipsterImmo
            - name: SPRING_DATASOURCE_USERNAME
              value: jhipsterImmo
            - name: SPRING_DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: jhipsterimmo-postgresql
                  key: postgresql-password
            - name: SPRING_DATA_JEST_URI
              value: http://jhipsterimmo-elasticsearch.prod-immo.svc.cluster.local:9200
            - name: SPRING_ELASTICSEARCH_REST_URIS
              value: http://jhipsterimmo-elasticsearch.prod-immo.svc.cluster.local:9200
            - name: JAVA_OPTS
              value: ' -Xmx256m -Xms256m'
          resources:
            requests:
              memory: '512Mi'
              cpu: '500m'
            limits:
              memory: '1Gi'
              cpu: '1'
          ports:
            - name: http
              containerPort: 8080
          readinessProbe:
            httpGet:
              path: /management/health
              port: http
            initialDelaySeconds: 20
            periodSeconds: 15
            failureThreshold: 6
          livenessProbe:
            httpGet:
              path: /management/info
              port: http
            initialDelaySeconds: 120
